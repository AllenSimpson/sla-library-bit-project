<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $librarian = factory(User::class)->create([
            'email' => 'admin@slalibrary.io',
            'role_id' => 1
        ]);

        $members = factory(User::class)->create([
            'role_id' => 2 
        ]);
    }
}
